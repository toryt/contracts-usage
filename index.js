#!/usr/bin/env node

/*
 Copyright 2016 - 2017 by Jan Dockx

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

const program = require("commander");
const Contract = require("@toryt/contracts-iv");

const fibonacciContract = new Contract({
  pre: [
    Number.isInteger,
    n => 0 <= n
  ],
  post:      [
    (n, result) => Number.isInteger(result),
    (n, result) => n !== 0 || result === 0,
    (n, result) => n !== 1 || result === 1,
    (n, result, fibonacci)  => n < 2 || result === fibonacci(n - 1) + fibonacci(n - 2)
  ],
  exception: Contract.mustNotHappen
})

const fibonacci = fibonacciContract.implementation(n => n <= 1 ? n : fibonacci(n - 1) + fibonacci(n - 2));

const cache = {}
cache[0] = 0
cache[1] = 1

const fibonacciOptimized = fibonacciContract.implementation(function(n) {
  if (cache[n]) {
    return cache[n];
  }

  for (let count = 2; count <= n; count++) {
    if (!Number.isFinite(cache[count - 1])) {
      return cache[count - 1]
    }
    cache[count] = cache[count - 1] + cache[count - 2]
  }
  return cache[n]
});

const factorial = new Contract({
  pre:       [
    Number.isInteger,
    n => 0 <= n
  ],
  post:      [
    (n, result) => Number.isInteger(result),
    (n, result) => n !== 0 || result === 1,
    // don't refer to a specific implementation in the Contract!
    (n, result, f) => n < 1 || result / f(n - 1) === n
  ],
  exception: [
    function() {return false;}
  ]
}).implementation(n => n <= 0 ? 1 : n * factorial(n - 1));

program
  .version("0.0.0");

program
  .command("fibonacci [n]")
  .alias("fib")
  .description("Calculate the fibonacci number of [n]")
  .action(function(n) {
    console.log(fibonacci(Number.parseInt(n)));
  });

program
  .command("fibonacci-verify [n]")
  .alias("fib-verify")
  .description("Calculate the fibonacci number of [n] (verified)")
  .action(function(n) {
    fibonacci.contract.verifyPostconditions = true
    console.log(fibonacci(Number.parseInt(n)));
  });

program
  .command("fibonaccis [n]")
  .alias("fibs")
  .description("Calculate the first [n] fibonacci numbers")
  .action(function(n) {
    for (let i = 0; i < n; i++) {
      console.log(fibonacci(i));
    }
  });

program
  .command("factorial [n]")
  .alias("fact")
  .description("Calculate the factorial of [n]")
  .action(function(n) {
    console.log(factorial(Number.parseInt(n)));
  });

program
  .command("fibonacci-optimized [n]")
  .alias("fib-opt")
  .description("Calculate the fibonacci number of [n] (optimized - Infinity when n >= 1477)")
  .action(function(n) {
    fibonacciOptimized.contract.verify = false
    console.log(fibonacciOptimized(Number.parseInt(n)));
  });

program.command("fibonacci-optimized-verified [n]")
  .alias("fib-opt-verified")
  .description("Calculate the fibonacci number of [n] (optimized - Infinity when n >= 1477 - verified)")
  .action(function(n) {
    fibonacciOptimized.contract.verifyPostconditions = true
    console.log(fibonacciOptimized(Number.parseInt(n)));
  });

program.parse(process.argv);
